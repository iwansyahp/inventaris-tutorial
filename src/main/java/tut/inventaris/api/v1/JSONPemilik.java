package tut.inventaris.api.v1;

import tut.inventaris.model.Pemilik;

public class JSONPemilik {
	
	private Pemilik pemilik;
	
	public String getNama() {
		if (pemilik == null) {
			return "";
		}
		return pemilik.getNama();
	}
	
	public Long getId() {
		if (pemilik == null) {
			return 0L;
		}
		return (Long) pemilik.getId();
	}
	
	public String getAlamat() {
		if (pemilik == null) {
			return "";
		}
		return pemilik.getAlamat();
	}
	
	
	
	// ambil resource lain disini pakai getter seperti pada 2 contoh getter diatas
	
	public JSONPemilik(Pemilik pemilik) {
		this.pemilik = pemilik;
	}
	
	public JSONPemilik() {
	}

}
