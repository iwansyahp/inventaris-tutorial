package tut.inventaris.api.v1;

import tut.inventaris.model.Pemilik;

public class JSONPemilikUpdate {
	// id dari pemilik yang ingin diubah
	public Long id;
	
	// informasi baru dari user yang akan diubah 
	public String namaBaru; 
	public String alamatBaru;
	public Integer umurBaru;
}
