package tut.inventaris.api.v1;

import java.util.ArrayList;
import java.util.List;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.config.Nullable;
import com.google.api.server.spi.response.NotFoundException;

import tut.inventaris.KonfigurasiDasar;
import tut.inventaris.model.Pemilik;
import tut.inventaris.model.PemilikCtrl;

@Api (name= "pemilik",
	title = "Layanan Pemilik",
	version="v1",
	description="API untuk resource pemilik")
public class PemilikAPI {
	
	// ini hanya untuk demo saja
	// pada aplikasi, kita tidak mungkin memberikan daftar user yang terdaftar melalui API
	@ApiMethod(name="daftar",
			path="pemilik",
			httpMethod=HttpMethod.GET)
	public List<JSONPemilik> daftar(
			@Nullable @Named("offset") Integer offset,
			@Nullable @Named("limit") Integer limit) throws NotFoundException {
		
		List<JSONPemilik> response = new ArrayList<JSONPemilik>();
		
		// periksa nilai offset dan limit
		// nanti bisa diterapkan nilai pagination yang berbeda untuk servlet dan api
		// dengan membuat variabel offset dan limit yang baru
		// ini hanya untuk demo
		if (offset == null || offset < 0) {
			offset = KonfigurasiDasar.OFFSET;
		}
		
		if (limit == null || limit < 0) {
			limit = KonfigurasiDasar.LIMIT;
		}
		
		List<Pemilik> daftarPemilik = new PemilikCtrl().list(offset, limit);
		
		if (daftarPemilik.isEmpty()) {
			throw new NotFoundException("Daftar pemilik kosong");
		}
		
		JSONPemilik jsonPemilik = new JSONPemilik();
		for(Pemilik p: daftarPemilik) {
			jsonPemilik = new JSONPemilik(p);
			response.add(jsonPemilik);
		}
		
		return response;
	}
	
	@ApiMethod(name="create",
			path="pemilik",
			httpMethod=HttpMethod.POST)
	public JSONPemilik baru(
			JSONPemilikCreate jsonPemilikCreate) throws Exception {
		JSONPemilik response = null;
		
		Pemilik pemilik = new PemilikCtrl().create(jsonPemilikCreate.nama, jsonPemilikCreate.alamat, jsonPemilikCreate.umur, jsonPemilikCreate.email, jsonPemilikCreate.password);
		
		response = new JSONPemilik(pemilik);
		
		return response;
	}
	
	@ApiMethod(name="get", 
			path="pemilik/{id}", 
			httpMethod=HttpMethod.GET)
	public JSONPemilik cari(
			@Named("id") Long id) {
		JSONPemilik response;
		
		Pemilik pemilik = 
				new PemilikCtrl().get(id);
		
		response = new JSONPemilik(pemilik);
		
		return response;
	}
	
	@ApiMethod(name="update", 
			path="pemilik", 
			httpMethod=HttpMethod.PUT)
	public JSONPemilik ubahNama(
			JSONPemilikUpdate jsonPemilikUpdate) throws Exception {
		JSONPemilik response = null;
		
		Long idB = (Long) Long.valueOf(jsonPemilikUpdate.id);
		Pemilik pemilik = new PemilikCtrl().update(jsonPemilikUpdate.id, jsonPemilikUpdate.namaBaru, 
				jsonPemilikUpdate.alamatBaru, jsonPemilikUpdate.umurBaru);
		
		response = new JSONPemilik(pemilik);
		
		return response;
	}
	
	@ApiMethod(name="delete", 
			path="pemilik/{id}", 
			httpMethod=HttpMethod.DELETE)
	public JSONPesan hapus(@Named("id") Long id) throws Exception {
		JSONPesan response = new JSONPesan("");
		
		new PemilikCtrl().delete(id);
		
		// pastikan sudah dihapus
		Pemilik pemilik = new PemilikCtrl().get(id);
		
		if(pemilik == null) {
			response.setMessage("Berhasil dihapus");
		} else {
			response.setMessage("Gagal dihapus");
		}
		
		return response;
	}
}
