package tut.inventaris.model;

import com.googlecode.objectify.annotation.Subclass;

@Subclass (index = true)
public class Kursi extends Inventaris {
	
	private String bahan;
	
	private String jenis;
	
	private Integer ruangDudukOrang; // jumlah orang yang dapat dimuat

	public String getBahan() {
		return bahan;
	}

	public void setBahan(String bahan) {
		this.bahan = bahan;
	}

	public String getJenis() {
		return jenis;
	}

	public void setJenis(String jenis) {
		this.jenis = jenis;
	}

	public Integer getRuangDudukOrang() {
		return ruangDudukOrang;
	}

	public void setRuangDudukOrang(Integer ruangDudukOrang) {
		this.ruangDudukOrang = ruangDudukOrang;
	}
	
	@SuppressWarnings("unused")
	private Kursi () {}

	public Kursi(String nama, Integer jumlah, String bahan, String jenis, Integer ruangDudukOrang) {
		
		super(nama, jumlah);
		
		this.bahan = bahan;
		this.jenis = jenis;
		this.ruangDudukOrang = ruangDudukOrang;
	}
	
}
