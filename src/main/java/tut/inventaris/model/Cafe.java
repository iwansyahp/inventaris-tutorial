package tut.inventaris.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

import java.lang.String;

@Entity
public class Cafe {

	@Id private Long id;

	@Index
	private String nama;
	
	@Load
	@Index
	private Ref<Pemilik> pemilik;
	
	private String alamat;
	
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}
	
	public Ref<Pemilik> getPemilik() {
		return pemilik;
	}

	public void setPemilik(Ref<Pemilik> pemilik) {
		this.pemilik = pemilik;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public Long getId() {
		return id;
	}
	
	@SuppressWarnings("unused")
	private Cafe() {}


	public Cafe(String nama, String alamat, Ref<Pemilik> pemilik) {
		this.nama = nama;
		this.alamat = alamat;
		this.pemilik = pemilik;
	}

}