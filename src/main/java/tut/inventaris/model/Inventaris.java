package tut.inventaris.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class Inventaris {
	
	@Id private Long id;
	
	private Integer jumlah;
	
	private String nama;

	public Integer getJumlah() {
		return jumlah;
	}

	public void setJumlah(Integer jumlah) {
		this.jumlah = jumlah;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public Long getId() {
		return id;
	}
	
	protected Inventaris(){}
	
	public Inventaris (String nama, Integer jumlah) {
		this.jumlah = jumlah;
		this.nama = nama;
	}
}
