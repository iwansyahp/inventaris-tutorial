package tut.inventaris.model;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Subclass;

@SuppressWarnings("serial")
@Subclass (index = true)
public class Pemilik extends Pemakai {
	
	@Index
	// daftar cafe yang dimiliki
	private List<Cafe> cafe;
	
	public List<Cafe> getCafe() {
		return cafe;
	}

	@SuppressWarnings("unused")
	private Pemilik () {}
	
	public Pemilik(String nama, String alamat, Integer umur, String email, byte[] salt, byte[] password) {
		// panggil super untuk buat pemakai
		super(nama, alamat, umur, email, salt, password);
		
		// secara default buat array kosong
		this.cafe = new ArrayList<>();
	}
	
}
