package tut.inventaris.model;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;

import tut.inventaris.KonfigurasiDasar;
import tut.inventaris.utils.SecureHashWithSalt;

// lihat docs ofy
import static com.googlecode.objectify.ObjectifyService.ofy;

public class PemilikCtrl {

	public List<Pemilik> list(int offset, int limit) {
		if (offset < 0) {
			offset = KonfigurasiDasar.OFFSET;
		}
		if (limit < 1) {
			limit = KonfigurasiDasar.LIMIT;
		}

		List<Pemilik> daftarPemilik = 
				ofy().load().type(Pemilik.class).offset(offset).limit(limit).list();

		if (daftarPemilik == null || daftarPemilik.size() < 1) {
			daftarPemilik = new ArrayList<>();
		}

		return daftarPemilik;
	}

	public Pemilik create(String nama, String alamat, Integer umur, String email, String password) 
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		

		// Pembangkitan password
		SecureHashWithSalt bangkitPassword = new SecureHashWithSalt();
		byte[] salt = bangkitPassword.buatSalt();
		byte[] passwordEnkripsi = bangkitPassword.enkripsi(password, salt);
		
		// buat entitas pemilik
		Pemilik pemilik = new Pemilik(nama, alamat, umur, email, salt, passwordEnkripsi);
		
		// simpan semua perubahan
		ofy().save().entity(pemilik).now();

		return pemilik;
	}

	/**
	 * Memperoleh salah satu cafe
	 * 
	 * @param id
	 * @return
	 */
	public Pemilik get(Long id) {
		// buat key dari id
		Key<Pemilik> key = Key.create(Pemilik.class, id);
		Pemilik pemilik = null;
		// baca docs safe()
		try {
			pemilik = ofy().load().key(key).safe();
		} catch (NotFoundException e) {
			// pastikan eksepsi yang ditangkap dari package objectify
			// tangani kesalahan
		}

		// versi lain bisa dibuat lebih singkat lihat update

		// atau pakai list. kemudian ambil elemen pertama

		return pemilik;
	}
	
	public Pemilik getByEmail(String email) {
		
		List<Pemilik> daftarPemilik = null;
		Pemilik pemilik = null;
		// baca docs safe()
		try {
			daftarPemilik = ofy().load().type(Pemilik.class).filter("email", email).list();
			// kalau email > lebih dari satu 
		} catch (NotFoundException e) {}
		
		if (daftarPemilik.size() < 1) {
			
		} else {
			pemilik = daftarPemilik.get(0);
		}
		return pemilik;
	}
	
	/**
	 * Ambil daftar cafe dari si pemilik
	 * @param id
	 * @return
	 */
	public List<Cafe> getCafe(Long id) {
		// buat key dari id
		Key<Pemilik> key = Key.create(Pemilik.class, id);
		List<Cafe> cafes = null;
		// baca docs safe()
		try {
			Pemilik pemilik = ofy().load().key(key).safe();
			cafes = pemilik.getCafe();
		} catch (NotFoundException e) {
			// pastikan eksepsi yang ditangkap dari package objectify
			// tangani kesalahan
		}

		// versi lain bisa dibuat lebih singkat lihat update

		// atau pakai list. kemudian ambil elemen pertama

		return cafes;
	}
	
	public List<Cafe> deleteCafe(Long idPemilik, Long idCafe) {
		// ambil info pemilik
		Pemilik pemilik = get(idPemilik);
		// buat key dari id cafe
		Cafe cafe = new CafeCtrl().get(idCafe);
		
		if (pemilik.getCafe() != null) {
			// hapus cafe dari daftar
			pemilik.getCafe().remove(cafe);
		}
		
		// simpan perubahan
		ofy().save().entity(pemilik);
		
		return pemilik.getCafe();
	}
	
	public List<Cafe> addCafe(Long idPemilik, Long idCafe) {
		// ambil info pemilik
		Pemilik pemilik = get(idPemilik);
		// buat key dari id cafe
		Cafe cafe = new CafeCtrl().get(idCafe);
		
		if (pemilik.getCafe() != null) {
			// tambah cafe baru ke dalam daftar
			pemilik.getCafe().add(cafe);
		}
		
		// simpan perubahan
		ofy().save().entity(pemilik);
		
		return pemilik.getCafe();
	}

	/**
	 * Mengubah informasi mengenai cafe
	 * 
	 * @param id
	 * @param namaBaru
	 * @param namaPemilikBaru
	 * @param alamatBaru
	 * @return
	 */
	public Pemilik update(Long id, String namaBaru, String alamatBaru, Integer umurBaru) {
		Pemilik pemilik = null;
		
		try {
			// ambil dari datastore
			pemilik = ofy().load().key(Key.create(Pemilik.class, id)).safe();
			// pastikan beda dan ubah
			if (!namaBaru.equals(pemilik.getNama())) {
				pemilik.setNama(namaBaru);
			}

			if (!alamatBaru.equals(pemilik.getAlamat())) {
				pemilik.setAlamat(alamatBaru);
			}

			// simpan semua perubahan
			ofy().save().entity(pemilik).now();
			
			// ambil cafe yang telah diubah
			pemilik = get(id);

		} catch (NotFoundException e) { }
		
		return pemilik;
	}

	/**
	 * Hapus cafe
	 * 
	 * @param id
	 */
	public void delete(Long id) {
		try {
			// ambil dari datastore
			Pemilik pemilik = ofy().load().key(Key.create(Pemilik.class, id)).safe();

			// hapus sekarang
			ofy().delete().entity(pemilik).now();

		} catch (NotFoundException e) {
		}
	}
}
