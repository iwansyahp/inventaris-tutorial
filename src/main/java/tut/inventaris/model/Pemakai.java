package tut.inventaris.model;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Pemakai implements Serializable {
	
	@Id
	private Long id;
	
	@Index
	private String nama;
	
	private String alamat;
	
	private Integer umur;
	
	@Index
	private String email;
	
	private byte[] salt; // salt untuk password
	
	private byte[] password; // kata kunci
	
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public Integer getUmur() {
		return umur;
	}

	public void setUmur(Integer umur) {
		this.umur = umur;
	}

	public Long getId() {
		return id;
	}
	
	public byte[] getSalt() {
		return salt;
	}

	public void setSalt(byte[] salt) {
		this.salt = salt;
	}

	public byte[] getPassword() {
		return password;
	}

	public void setPassword(byte[] password) {
		this.password = password;
	}

	protected Pemakai () {}

	public Pemakai(String nama, String alamat, Integer umur, String email, byte[] salt, byte[] password) {
		this.nama = nama;
		this.alamat = alamat;
		this.umur = umur;
		this.email = email;
		this.salt = salt;
		this.password = password;
	}
	
	private static final long serialVersionUID = -1656003176243785554L;
	
}
