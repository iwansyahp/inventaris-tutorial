package tut.inventaris.model;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.googlecode.objectify.Ref;

import tut.inventaris.KonfigurasiDasar;

// lihat docs ofy
import static com.googlecode.objectify.ObjectifyService.ofy;

public class CafeCtrl {

	public List<Cafe> list(int offset, int limit) {
		if (offset < 0) {
			offset = KonfigurasiDasar.OFFSET;
		}
		if (limit < 1) {
			limit = KonfigurasiDasar.LIMIT;
		}

		List<Cafe> cafes = ofy().load().type(Cafe.class).offset(offset).limit(limit).list();

		if (cafes == null || cafes.size() < 1) {
			cafes = new ArrayList<>();
		}

		return cafes;
	}
	
	public List<Cafe> listByOwner(int offset, int limit, Long idPemilik) {
		if (offset < 0) {
			offset = KonfigurasiDasar.OFFSET;
		}
		if (limit < 1) {
			limit = KonfigurasiDasar.LIMIT;
		}
		// ambil info pemilik cafe
		Pemilik pemilik = new PemilikCtrl().get(idPemilik);
		Ref<Pemilik> refPemilik = Ref.create(pemilik);
		List<Cafe> cafePemilik = ofy().load().type(Cafe.class).filter("pemilik =", refPemilik).list();
		
		return cafePemilik;
	}

	public Cafe create(String nama, Long idPemilik, String alamat) {
		// buat entitas cafe
		Key<Pemilik> keyPemilik = Key.create(Pemilik.class, idPemilik);
		Ref<Pemilik> refPemilik = Ref.create(keyPemilik);
		// entitas cafe yang baru
		Cafe cafe = new Cafe(nama, alamat, refPemilik);
		
		// tambah cafe ini ke Pemilik.cafe
		// simpan semua perubahan
		ofy().save().entity(cafe).now();

		return cafe;
	}

	/**
	 * Memperoleh salah satu cafe
	 * 
	 * @param id
	 * @return
	 */
	public Cafe get(Long id) {
		// buat key dari id
		Key<Cafe> key = Key.create(Cafe.class, id);
		Cafe cafe = null;
		// baca docs safe()
		try {
			cafe = ofy().load().key(key).safe();
		} catch (NotFoundException e) {
			// pastikan eksepsi yang ditangkap dari package objectify
			// tangani kesalahan
		}

		// versi lain bisa dibuat lebih singkat lihat update

		// atau pakai list. kemudian ambil elemen pertama

		return cafe;
	}

	public List<Cafe> getByName(String nameToFind, int offset, int limit) {
		// inisialisasi
		List<Cafe> cafes = new ArrayList<>();

		if (offset < 0) {
			offset = KonfigurasiDasar.OFFSET;
		}
		if (limit < 1) {
			limit = KonfigurasiDasar.LIMIT;
		}

		cafes = ofy().load().type(Cafe.class).filter("nama", nameToFind).offset(offset).limit(limit).list();

		return cafes;
	}

	/**
	 * Mengubah informasi mengenai cafe
	 * 
	 * @param id
	 * @param namaBaru
	 * @param namaPemilikBaru
	 * @param alamatBaru
	 * @return
	 */
	public Cafe update(Long id, String namaBaru, Long idPemilikBaru, String alamatBaru) {
		Cafe cafe = null;
		
		try {
			// ambil dari datastore
			cafe = ofy().load().key(Key.create(Cafe.class, id)).safe();
			// pastikan beda dan ubah
			if (!namaBaru.equals(cafe.getNama())) {
				cafe.setNama(namaBaru);
			}
			
			// ambil info pemilik baru
			Key<Pemilik> keyPemilikBaru = Key.create(Pemilik.class, idPemilikBaru);
			Ref<Pemilik> refPemilikBaru = Ref.create(keyPemilikBaru);
			
			// bandingkan info pemilik lama dan baru
			if (!refPemilikBaru.equals(cafe.getPemilik())) {
				
				// ambil info pemilik lama
				Pemilik pemilikLama = new PemilikCtrl().get(cafe.getPemilik().get().getId());
				
				// hapus cafe dari pemilik lama
				new PemilikCtrl().deleteCafe(pemilikLama.getId(), id);
				
				// masukkan info pemilik baru ke entitas cafe
				cafe.setPemilik(refPemilikBaru);
			}

			if (!alamatBaru.equals(cafe.getAlamat())) {
				cafe.setAlamat(alamatBaru);
			}

			// simpan semua perubahan
			ofy().save().entity(cafe).now();
			
			// ambil cafe yang telah diubah
			cafe = get(id);

		} catch (NotFoundException e) { }
		
		return cafe;
	}
	
	public Cafe updateCafe(Long id, String namaBaru, String alamatBaru) {
		Cafe cafe = null;
		
		try {
			// ambil dari datastore
			cafe = ofy().load().key(Key.create(Cafe.class, id)).safe();
			// pastikan beda dan ubah
			if (!namaBaru.equals(cafe.getNama())) {
				cafe.setNama(namaBaru);
			}

			if (!alamatBaru.equals(cafe.getAlamat())) {
				cafe.setAlamat(alamatBaru);
			}

			// simpan semua perubahan
			ofy().save().entity(cafe).now();
			
			// ambil cafe yang telah diubah
			cafe = get(id);

		} catch (NotFoundException e) { }
		
		return cafe;
	}

	/**
	 * Hapus cafe
	 * 
	 * @param id
	 */
	public void delete(Long id) {
		try {
			// ambil dari datastore
			Cafe cafe = ofy().load().key(Key.create(Cafe.class, id)).safe();
			
			// hapus cafe dari pemilik yang terdaftar
			new PemilikCtrl().deleteCafe(cafe.getPemilik().get().getId(), id);
			
			// hapus sekarang
			ofy().delete().entity(cafe).now();

		} catch (NotFoundException e) {
		}
	}
}
