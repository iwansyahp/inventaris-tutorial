package tut.inventaris.model;

import com.googlecode.objectify.annotation.Subclass;

@Subclass (index = true)
public class Susu extends Inventaris {
	
	private String jenis; // kental manis atau full cream
	
	private String merk;
	
	private String varianRasa;
		
	public String getJenis() {
		return jenis;
	}

	public void setJenis(String jenis) {
		this.jenis = jenis;
	}
	
	public String getMerk() {
		return merk;
	}

	public void setMerk(String merk) {
		this.merk = merk;
	}

	public String getVarianRasa() {
		return varianRasa;
	}

	public void setVarianRasa(String varianRasa) {
		this.varianRasa = varianRasa;
	}

	@SuppressWarnings("unused")
	private Susu () {}

	public Susu(String nama, Integer jumlah, String merk, String varianRasa) {
		super(nama, jumlah);
		this.merk = merk;
		this.varianRasa = varianRasa;
	}
	
}
