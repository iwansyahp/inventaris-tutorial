package tut.inventaris.ctrl.auth;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import tut.inventaris.model.Pemilik;
import tut.inventaris.model.PemilikCtrl;
import tut.inventaris.utils.SecureHashWithSalt;

public class LoginAuthenticator {
	
	/**
	 * Periksa status login
	 * @param email
	 * @param password
	 * @return false >> gagal login. true >> berhasil login
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeySpecException 
	 */
	public Boolean berhasilLogin (String email, String password) 
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		
		Pemilik pemilik = null;
		
		pemilik = new PemilikCtrl().getByEmail(email);
		
		// email tidak terdaftar
		if (pemilik == null) {
			return false;
		} else {
			// bandingkan password yang dimasukkan dengan password yang telah disimpan

			// Bandingkan password
			SecureHashWithSalt bangkitPassword = new SecureHashWithSalt();
			if (bangkitPassword.sama(password, pemilik.getPassword(), pemilik.getSalt())) {
				return true;
			}
		}
		
		return false;
	}
}
