package tut.inventaris.ctrl.auth;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;

import tut.inventaris.ctrl.KonfigurasiJSP;
import tut.inventaris.ctrl.PeriksaLogin;
import tut.inventaris.model.Pemilik;
import tut.inventaris.model.PemilikCtrl;

@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		PeriksaLogin periksaLogin = new PeriksaLogin();
		
		// periksa apakah sudah melakukan login
		if (periksaLogin.sudahLogin(request)) {
			response.sendRedirect("/cafe");
		}
		
		String googleLoginUrl = UserServiceFactory.getUserService().createLoginURL("/cafe");
		// ambil info user google sekarang
		//User googleUser = UserServiceFactory.getUserService().getCurrentUser();
		
		request.setAttribute("googleLoginURL", googleLoginUrl);
		//request.setAttribute("googleUser", googleUser);
		
		response.setContentType("text/html");
		RequestDispatcher jsp = request
				.getRequestDispatcher(KonfigurasiJSP.LOGIN_PEMILIK_PATH);
		jsp.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		// periksa info login
		try {
			Boolean berhasilLogin = new LoginAuthenticator().berhasilLogin(email, password);
			
			HttpSession session = null;
			// jika login berhasil redirect ke url ini kembali
			if (berhasilLogin) {

				Pemilik pemilik = new PemilikCtrl().getByEmail(email);
				
				// true >> dibuat sesi baru
				// false >> diambil sesi yang sudah ada
				// req.getSession().invalidate() >> hapus sesi
				session = request.getSession(true);
				session.setAttribute("idPemakai", pemilik.getId());
				session.setAttribute("pemakai", pemilik);
				
				// Pastikan lama tidak aktif sebelum dipaksa logout
				session.setMaxInactiveInterval(60 * 60 * 8); // 8 jam dalam detik
				
				response.sendRedirect("/cafe");
				
			} else {
				response.sendRedirect("/login");
			}
			
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
