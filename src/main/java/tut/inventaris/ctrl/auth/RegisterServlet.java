package tut.inventaris.ctrl.auth;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tut.inventaris.ctrl.KonfigurasiJSP;
import tut.inventaris.ctrl.PeriksaLogin;
import tut.inventaris.model.PemilikCtrl;

@SuppressWarnings("serial")
public class RegisterServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		PeriksaLogin periksaLogin = new PeriksaLogin();

		if (periksaLogin.sudahLogin(request)) {
			response.sendRedirect("/cafe");
		}

		response.setContentType("text/html");
		RequestDispatcher jsp = request
				.getRequestDispatcher(KonfigurasiJSP.REGISTER_PEMILIK_PATH);
		jsp.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		String nama = request.getParameter("nama");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String alamat = request.getParameter("alamat");
		 
		// TODO: periksa email sudah terdaftar atau belum 
		try {
			// buat user baru
			new PemilikCtrl().create(nama, alamat, 10, email, password);
			
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		response.sendRedirect("/login");
	}
}
