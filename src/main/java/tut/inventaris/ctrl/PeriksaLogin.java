package tut.inventaris.ctrl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import tut.inventaris.model.Pemakai;

public class PeriksaLogin {
	
	public boolean sudahLogin(HttpServletRequest req) {

		if (googleLogin()) {
			return true;
		}
		// Periksa apakah admin google 
		Pemakai pemakaiLogin = null;

		// ambil info sesi saat ini
		HttpSession session = req.getSession(false);
				if (session != null) { 
			pemakaiLogin = (Pemakai) session.getAttribute("pemakai");
			if (pemakaiLogin != null) { // Session object pemakai tidak ada berarti 
								   // belum login
				if (!pemakaiLogin.getId().toString().equals("")) { 
						return true;
				}
			}				
		}
				
		return false;
	}
	
	public boolean googleLogin() {
		// periksa apakah pemakai login menggunakan akun Google
		UserService userService = UserServiceFactory.getUserService();
		if (userService != null) {
			// pengguna sudah login
			if (userService.isUserLoggedIn()) {
				if (userService.isUserAdmin())
					return true;
			}
		}
		return false;		
	}
}
