package tut.inventaris.ctrl;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tut.inventaris.model.Cafe;
import tut.inventaris.model.CafeCtrl;

@SuppressWarnings("serial")
public class UbahCafeServlet extends HttpServlet {
       
	Long id = null;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		PeriksaLogin periksaLogin = new PeriksaLogin();

		if (!periksaLogin.sudahLogin(request)) {
			response.sendRedirect("/login");
		}
		
		// ambil daftar cafe
		id = Long.valueOf(request.getParameter("id"));
		
		Cafe cafe= null; 
		cafe = new CafeCtrl().get(id);
		
		if (cafe != null) {
			request.setAttribute("cafe", cafe);	
		}
		
		response.setContentType("text/html");
		RequestDispatcher jsp = request
				.getRequestDispatcher(KonfigurasiJSP.UBAH_CAFE_PATH);
		jsp.forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		String nama = request.getParameter("nama");
		
		@SuppressWarnings("unused")
		String namaPemilik = request.getParameter("namaPemilik");
		
		String alamat = request.getParameter("alamat");
		
		new CafeCtrl().updateCafe(id, nama, alamat);
		
		// redirect ke url ini kembali
		response.sendRedirect("/cafe");
	}

}
