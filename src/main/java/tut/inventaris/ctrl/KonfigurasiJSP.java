package tut.inventaris.ctrl;

public class KonfigurasiJSP {
	// path
	public static final String JSP_PATH = "/WEB-INF/jsp";
	
	public static final String TAMBAH_CAFE_PATH = "/WEB-INF/jsp/tambah_cafe.jsp";
	public static final String UBAH_CAFE_PATH = "/WEB-INF/jsp/ubah_cafe.jsp";
	public static final String HAPUS_CAFE_PATH = "/WEB-INF/jsp/hapus_cafe.jsp";
	
	// auth
	public static final String REGISTER_PEMILIK_PATH = "/WEB-INF/jsp/register_pemilik.jsp";
	public static final String LOGIN_PEMILIK_PATH = "/WEB-INF/jsp/login_pemilik.jsp";
}