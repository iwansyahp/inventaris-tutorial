package tut.inventaris.ctrl;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import tut.inventaris.KonfigurasiDasar;
import tut.inventaris.model.Cafe;
import tut.inventaris.model.CafeCtrl;
import tut.inventaris.model.Pemilik;
import tut.inventaris.model.PemilikCtrl;

@SuppressWarnings("serial")
public class CafeServlet extends HttpServlet {

	// ambil user login saat ini
	Long idPemakai = null;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		PeriksaLogin periksaLogin = new PeriksaLogin();

		if (!periksaLogin.sudahLogin(request)) {
			response.sendRedirect("/login");
		}
		
		// ambil info pemilik cafe
		idPemakai = (Long) request.getSession().getAttribute("idPemakai");
		Pemilik pemilik = (Pemilik) request.getAttribute("pemakai");

		// ambil daftar cafe pemakai
		List<Cafe> cafes = null;
		
		// jika pemakai login pakai akun google dan sebagai admin
		// ambil seluruh cafe
		if (idPemakai == null) {
			cafes = new CafeCtrl().list(0, 20); 
		} else {
			cafes = new CafeCtrl().listByOwner(KonfigurasiDasar.OFFSET, KonfigurasiDasar.LIMIT, idPemakai);
		}
		
		if (cafes != null) {
			request.setAttribute("cafes", cafes);
		}
		
		String googleLogoutURL = null;
		googleLogoutURL = UserServiceFactory.getUserService().createLogoutURL("/login");
		
		// Snippet untuk ambil info user
		User user = UserServiceFactory.getUserService().getCurrentUser();
		// misal ambil email
		String emailUserGoogle = ""; 
		if (user != null) {
			emailUserGoogle  = user.getEmail();	
		}
		
		if (periksaLogin.googleLogin()) {
			request.setAttribute("googleLogoutURL", googleLogoutURL);
			request.setAttribute("emailG", emailUserGoogle);
		}
		
		response.setContentType("text/html");
		RequestDispatcher jsp = request
				.getRequestDispatcher(KonfigurasiJSP.TAMBAH_CAFE_PATH);
		jsp.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		String nama = request.getParameter("nama");
		
		String alamat = request.getParameter("alamat");
				
		// ambil info pemilik yang sedang login
		Pemilik pemilik = new PemilikCtrl().get(idPemakai);
		
		new CafeCtrl().create(nama, pemilik.getId(), alamat);
		
		// redirect ke url ini kembali
		response.sendRedirect("/cafe");
	}

}
