<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page session="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Mengubah Cafe Baru</title>
</head>
	<form action="/cafe/ubah" method="post">
	<label>Nama </label>
	<input name="nama" value="${cafe.nama}">
	<br>
	<label>Nama Pemilik </label><input name="namaPemilik" value="${cafe.pemilik.value.nama}" disabled>
	<br>
	<label>Alamat </label><input name="alamat" value="${cafe.alamat}">
	<button type="submit">Ubah</button>
	</form>
</body>
</html>