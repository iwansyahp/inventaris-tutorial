<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page session="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Menambahkan Cafe Baru</title>
</head>
<table border="2">
	<tr>
		<td>Nama Cafe</td>
		<td>Nama Pemilik</td>
		<td>Alamat</td>
		<td>Aksi</td>
	</tr>
	<c:forEach var="cafe" items="${cafes}">
	<tr>
		<td>${cafe.nama}</td>
		<td>${cafe.pemilik.value.nama}</td>
		<td>${cafe.alamat}</td>	
		<td>
			<a href="/cafe/ubah?id=${cafe.id}">Ubah </a>
			<br>
			<a href="/cafe/hapus?id=${cafe.id}">Hapus</a>
		</td>
		<tr>
	</c:forEach>
	</table>
	<form action="/cafe" method="post">
	<label>Nama </label>
	<input name="nama">
	<br>
	<label>Alamat </label><input name="alamat">
	<button type="submit">Tambah</button>
	</form>
	
	<br>
	<br>
	<c:choose>
	<c:when test="${not empty emailG}">
			<p>${emailG}</p>
	</c:when>
	</c:choose>
	<c:choose>
	<c:when test="${not empty googleLogoutURL}">
		<a href="${googleLogoutURL}">Keluar dari App (Google)</a>
	</c:when>
	<c:otherwise>
		<a href="/logout">Keluar dari App</a>
	</c:otherwise>
	</c:choose>
</body>
</html>