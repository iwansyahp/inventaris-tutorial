# Langkah - langkah yang diperlukan dalam mengembangkan App Engine (Java)

A. Tools 
1 . Tools yang dibutuhkan
	- Java Development Kit (1.8 / 1.7)
	- Google Cloud SDK
	- Eclipse Java EE
	- Google Cloud Tools SDK

B. Step by Step Process
1. Pembuatan aplikasi GAE pada Eclipse
	- File - New - Other - Google Cloud Platform - Google App Engine Standard Project
	- Ketikkan nama proyek dan package
	- Tambahkan Objectify dan Google Cloud Endpoints ke Build path
	- Buat package untuk masing-masing komponen app (model, ctrl, util, dll)
	- buat folder jsp di webapp/WEB-INF

2. Coba Jalankan aplikasi pada local server
	- Run - Run As - App Engine

Langkah 3-x lihat di https://cloud.google.com/appengine/docs/standard/java/gettingstarted/using-datastore-objectify (dengan penyesuaian)

3. Pembuatan Entitas Datastore
	- Entitas
	- Helper
	- Pemetaan helper di servlet routing
	- Buat ctrl untuk masing-masing model

4. Daftar Entitas
Cafe 
'Pemakai' superclass 'Pemilik' (polymorphisme)
Inventaris superclass Meja, Kursi, Gelas, Piring (1-m, n-m)
Makanan Minuman


C. Other
1. Tutorial lainnya dapat belajar di https://codelabs.developers.google.com/
2. Dokumentasi Google Cloud Endpoint >> https://cloud.google.com/endpoints/docs/frameworks/
3. Pencarian pakai 'like' di Datastore: https://cloud.google.com/appengine/docs/standard/java/search/
